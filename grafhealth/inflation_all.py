# SPDX-FileCopyrightText: 2023-2024 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

"""Functions to find inflated relays."""
import logging

import grafanalib.core as G

from grafhealth import common

from . import config

logger = logging.getLogger(__name__)

GSQL_INFLATION_VIEWS = [
    "lowest_ratio_highest_advertised_bw",
    "lowest_ratio_highest_weight",
    "highest_advertised_bw_lowest_ratio",
    "highest_weight_lowest_ratio",
    "weight_gt_stream",
    "stream_gt_throughput",
]


def rows_table_flag(query, query_fps):
    query = (
        f"SELECT * FROM {query}"
        + "('${__from:date:iso}', '${__to:date:iso}', ${scanner_country})"
    )
    panels = []
    target = G.SqlTarget(
        rawSql=query,
        format="table",
        datasource=config.datasource_pg,
    )
    panel = G.Table(
        dataSource=config.datasource_pg,
        title=f"{query}",
        targets=[target],
    )
    panels.append(panel)
    flags_query = (
        """SELECT * FROM sum_relays_flag(
            '${__from:date:iso}', '${__to:date:iso}',"""
        + f"array({query_fps}))"
    )
    target = G.SqlTarget(
        rawSql=flags_query,
        format="table",
        datasource=config.datasource_pg,
    )
    panel = G.PieChartv2(
        dataSource=config.datasource_pg,
        title="Flags",
        targets=[target],
    )
    panels.append(panel)
    row = G.Row(
        panels=panels,
    )
    return [row]


def dashboards_inflation_all():
    """Create grafana dashboard with panels comparing bandwidth histories.

    With observed and stream bandwidth for several relays.
    """
    logger.info("Creating grafana Inflation detection dashboards.")
    dashboards = []
    for query in GSQL_INFLATION_VIEWS:
        fps = (
            f"SELECT fingerprint from {query}"
            + "('${__from:date:iso}', '${__to:date:iso}', ${scanner_country})"
        )
        rows = [common.net_stream_bw_avg_row()]
        rows.append(common.net_ratio_avg_row())
        rows.extend(rows_table_flag(query, fps))
        rows_bwvalues = common.bandwidth_values_inflation_rows()
        rows.extend(rows_bwvalues)
        templating = common.templating_vars_fp_node_id_nick(fps)
        title = f"Inflation detection, {query}"
        uid = "inflation-{}".format(query.replace("_", "-"))[:40]
        description = "Inflation detection, tpo/network-health/team#313"
        ds = common.create_dashboard(title, uid, rows, description, templating)
        dashboards.append(ds)
    return dashboards
