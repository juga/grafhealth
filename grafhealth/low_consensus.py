# SPDX-FileCopyrightText: 2023-2024 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

"""Functions for Low consensus grafana dashboard."""
import logging

from grafhealth import common

logger = logging.getLogger(__name__)

NTH_GROUPS = {
    "NTH25": [f"NTH25R{i}" for i in range(1, 9)],
    "NTH26": [f"NTH26R{i}" for i in range(1, 9)],
    "NTH27": [f"NTH27R{i}" for i in range(1, 9)],
    "NTH37": [f"NTH37R{i}" for i in range(1, 2)],
    "NTH38": ["NTH25R8"],
}


def dashboards_low_consensus():
    """Create grafana dashboard with panels comparing bandwidth histories.

    With observed and stream bandwidth for several relays.
    """
    logger.info("Creating grafana Low consensus dashboards.")
    rows = common.bandwidth_values_inflation_rows()
    nicks = []
    [nicks.extend(i) for i in NTH_GROUPS.values()]
    query = (
        "SELECT DISTINCT fingerprint FROM bw_line WHERE nick in ({})".format(
            ",".join(["'{}'".format(i) for i in nicks])
        )
    )
    logger.debug(f"fps query: {query}")
    templating = common.templating_vars_fp_node_id_nick(query)
    uid = "low-consensus-nth"
    title = "Low consensus, nth"
    ds = common.create_dashboard(title, uid, rows, templating=templating)
    return [ds]
