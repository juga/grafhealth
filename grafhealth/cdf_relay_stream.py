# SPDX-FileCopyrightText: 2023-2024 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

# For tpo/network-health/monitoring-and-alerting#29:
# Start monitoring stream CDF ratio graphs
# ----------------------------------------

import logging

import grafanalib.core as G

from grafhealth import common

from . import config

logger = logging.getLogger(__name__)

GSQL_FN_BW_MEAN = """
SELECT {}(bandwidth_record.bw_mean) AS {}
FROM bandwidth_record
INNER JOIN bandwidth_file,
WHERE $__timeFilter(time)
{}"""
# GSQL_RATIO_CDF_BWAUTH = """
# SELECT
#     GREATEST(r_strm, r_strm_filt) AS ratio,
#     cume_dist() over (
#         ORDER BY GREATEST(r_strm, r_strm_filt)
#     ) AS cdf_{}
# FROM bandwidth_record
# INNER JOIN bandwidth_file,
# WHERE $__timeFilter(time)
# """

GSQL_RATIO_CDF_BWAUTH = """
SELECT
    ratio,
    cume_dist() over (
        ORDER BY ratio
    ) AS cdf_{}
FROM bw_line
WHERE $__timeFilter(time)
{}
ORDER BY ratio"""

GSQL_FN_BW_MEAN = """
SELECT {}(stream_bw) AS {}
FROM bw_line
WHERE $__timeFilter(time)
{}"""


def ratio_cdf_rows():
    """Create grafana rows with Trend panels, one for each bwauth CDF."""
    panels = []
    rows = []
    # Remove moria1 since it doesn't have ratio
    bwauths = common.FILTER_BWAUTHS
    bwauths.pop("moria1")
    for bwauth, bwauth_sql in bwauths.items():
        targets = []
        query = GSQL_RATIO_CDF_BWAUTH.format(
            bwauth,
            bwauth_sql,
        )
        logger.debug(f"{query}")
        target = G.SqlTarget(
            rawSql=query,
            format="table",
            datasource=config.datasource_pg,
        )
        targets.append(target)
        panel = common.Trend(
            title=f"Relays CDF ratio {bwauth}",
            targets=targets,
            dataSource=config.datasource_pg,
            # yAxes=G.single_y_axis(label="percent", max=3),
            extraJson={
                "fieldConfig": {
                "overrides": [{
                    "matcher": {
                    "id": "byName",
                    "options": "ratio"
                    },
                    "properties": [
                    {
                        "id": "max",
                        "value": 3
                    }
                    ]
                }]
            }}
        )
        if len(panels) < 5:
            panels.append(panel)
        else:
            row = G.Row(panels=panels)
            rows.append(row)
            panels = [panel]
    row = G.Row(panels=panels)
    if row not in rows:
        rows.append(row)
    return rows


def fn_rows():
    """Create grafana rows with PieChart panels for each bwauth function."""
    panels = []
    rows = []
    for fn in ["MAX", "AVG", "STDDEV"]:
        targets = []
        logger.debug("fn: %s", fn)
        for bwauth, bwauth_sql in common.FILTER_BWAUTHS.items():
            logger.debug("bwauth: %s", bwauth)
            query = GSQL_FN_BW_MEAN.format(fn, bwauth, bwauth_sql)
            logger.debug(f"{query}")
            target = G.SqlTarget(
                rawSql=query,
                format="table",
                datasource=config.datasource_pg,
            )
            targets.append(target)
        panel = G.PieChartv2(
            title=f"{fn}(stream bandwidth)",
            targets=targets,
            dataSource=config.datasource_pg,
        )
        if len(panels) < 3:
            panels.append(panel)
        else:
            row = G.Row(panels=panels)
            rows.append(row)
            panels = [panel]
    row = G.Row(panels=panels)
    if row not in rows:
        rows.append(row)
    return rows


def dashboards_ratio_cdf():
    """Create grafana CDF Relays Stream Capacity (ratio) dashboards."""
    logger.info("Creating CDF ratio dashboards.")
    rows = fn_rows()
    rows.extend(ratio_cdf_rows())
    title = "CDF Relays Stream Capacity (ratio)"
    uid = "cdf"
    ds = common.create_dashboard(title, uid, rows)
    return [ds]
