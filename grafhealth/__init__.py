# SPDX-FileCopyrightText: 2023-2024 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

from grafhealth._config import Config

from . import _version

__version__ = _version.get_versions()["version"]
config = Config()
