#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023-2024 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

"""graphealth CLI to generate grafana dashboards.

For some issues in network-health
"""
import argparse
import datetime
import logging
import logging.config
import os.path

from grafhealth import (
    __version__,
    bwvalues,
    cdf_relay_stream,
    churn,
    common,
    config,
    defaults,
    histogram_ratio,
    inflation_all,
    low_consensus,
    util,
)

logging.config.dictConfig(defaults.LOGGING)
logger = logging.getLogger(__name__)


def create_parser():
    """Create arguments parser."""
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description=__doc__,
    )
    parser.add_argument(
        "--version",
        action="version",
        help="grafhealth version",
        version=f"{__version__}",
    )
    parser.add_argument(
        "-a",
        "--all",
        action="store_true",
        help="Update or create all dashboards.",
    )
    parser.add_argument(
        "-i",
        "--inflation",
        action="store_true",
        help="Update or create Inflation detection grafana dashboards.",
    )
    # parser.add_
    parser.add_argument(
        "-c",
        "--cdf-ratio",
        action="store_true",
        help="Update or create Relays stream capacity (ratio) CDF grafana "
        "dashboard.",
    )
    parser.add_argument(
        "-t",
        "--histogram-ratio",
        action="store_true",
        help="Update or create Relays ratio histogram grafana dashboard.",
    )
    parser.add_argument(
        "-r",
        "--stream-by-weight",
        action="store_true",
        help="Update or create Relays' stream bandwidth by weight grafana "
        "dashboard.",
    )
    parser.add_argument(
        "-b",
        "--bandwidth-values",
        action="store_true",
        help="Update or create Relays' bandwidth values grafana dashboard.",
    )
    parser.add_argument(
        "-l",
        "--low-consensus",
        action="store_true",
        help="Update or create Low consensus weight grafana dashboards.",
    )
    parser.add_argument(
        "-u",
        "--churn",
        action="store_true",
        help="Update or create Relays' churn grafana dashboards.",
    )
    parser.add_argument(
        "-d",
        "--days_back",
        default=defaults.DAYS_BACK,
        type=int,
        help="Set a date interval with x days back for the grafana "
        "dashboards.",
    )
    parser.add_argument(
        "-e",
        "--end",
        type=util.valid_datetime,
        help="Set the end of the date/time interval for the grafana "
        "dashboards.",
    )
    parser.add_argument(
        "-s",
        "--start",
        type=util.valid_datetime,
        help="Set the start of the date/time interval for the grafana "
        "dashboards.",
    )
    parser.add_argument(
        "-w",
        "--write",
        action="store_true",
        help="Write the dashborads in JSON files.",
    )
    parser.add_argument(
        "-p",
        "--provisioned",
        action="store_true",
        help="Write the dashborads in JSON files.",
    )
    return parser


class Grafhealth:
    def __init__(self):
        self.args = None
        self.dashboards = []

    def set_args(self, args):
        self.args = args

    def set_config_date_interval(self):
        now = datetime.datetime.utcnow()
        if self.args.start:
            if self.args.end:
                date_interval = (self.args.start, self.args.end)
            else:
                date_interval = (
                    self.args.start.strftime(defaults.GRAFANA_DATETIME_FMT),
                    now.strftime(defaults.GRAFANA_DATETIME_FMT),
                )
        elif self.args.days_back:
            date_interval = util.days_back2grafana_time(self.args.days_back)
        else:
            date_interval = util.days_back2grafana_time(defaults.DAYS_BACK)
        logger.debug(f"{date_interval}")
        config.set_date_interval(date_interval)
        return date_interval

    def set_config_provisioned(self):
        if getattr(self.args, "provisioned", None):
            config.set_provisioned(self.args.provisioned)

    def create_dashboards(self):
        dashboard_types = {
            "inflation": inflation_all.dashboards_inflation_all,
            "cdf_ratio": cdf_relay_stream.dashboards_ratio_cdf,
            "histogram_ratio": histogram_ratio.dashboards_ratio_histogram,
            # "stream_by_weight",
            "low_consensus": low_consensus.dashboards_low_consensus,
            "bandwidth_values": bwvalues.dashboards_bandwith_values,
            "churn": churn.dashboards_churn,
        }
        all_types = getattr(self.args, "all", None)
        for ds_type, ds_fn in dashboard_types.items():
            if all_types or getattr(self.args, ds_type, None):
                self.dashboards.extend(ds_fn())

    def write_dashboards_json(self, dashboards_dirpath=defaults.GRAFANA_PATH):
        if not getattr(self.args, "write", None):
            return
        now = datetime.datetime.utcnow()
        now_str = now.strftime("%Y_%m_%d_%H_%M_%S")
        for ds in self.dashboards:
            # When manually copy/pasting dashboards, remote grafana server
            # doesn't create the dashboard when it has the key `dashboard`,
            # so get the inside of this key to write to file.
            dashboard_json = common.get_inside_dashboards_json(ds)
            dashboard_file = f"{now_str}_{ds.uid}.json"
            dashboard_fpath = os.path.join(dashboards_dirpath, dashboard_file)
            with open(dashboard_fpath, "w", encoding="utf-8") as fd:
                fd.write(dashboard_json)

    def upload_dashboards_grafana_server(self):
        for ds in self.dashboards:
            dashboard_json_str = common.get_dashboards_json(
                ds, overwrite=True, folder_uid=config.folder_uid
            )
            common.upload_to_grafana(
                dashboard_json_str, config.server, config.api_key
            )


def main():
    parser = create_parser()
    args = parser.parse_args()
    grafhealth = Grafhealth()
    grafhealth.set_args(args)
    grafhealth.set_config_date_interval()
    grafhealth.set_config_provisioned()
    grafhealth.create_dashboards()
    grafhealth.write_dashboards_json()
    grafhealth.upload_dashboards_grafana_server()


if __name__ == "__main__":
    main()
