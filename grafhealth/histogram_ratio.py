# SPDX-FileCopyrightText: 2023-2024 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

# For tpo/network-health/monitoring-and-alerting#31:
# Create ratio histogram graphs
# -----------------------------
import logging

import grafanalib.core as G

from grafhealth import common

from . import config

logger = logging.getLogger(__name__)

GSQL_FP_RATIO_BWAUTH = """SELECT
    fingerprint,
    AVG(ratio) AS ratio_{}
FROM bw_line
WHERE $__timeFilter(time)
{}
GROUP BY fingerprint
ORDER BY ratio_{}"""


def query_ratio_histogram_rows():
    """Create grafana rows with Histogram panels.

    One for each bwauth histogram
    """
    rows = []
    panels = []
    # Create an histogram panel wit all bwauths
    # Remove moria1 cause it doesn't have ratio
    bwauths = common.FILTER_BWAUTHS
    bwauths.pop("moria1")
    # Create an histogram panel wit all bwauths
    bwauths[""] = ""
    for bwauth, bwauth_sql in bwauths.items():
        logger.debug("bwauth: %s", bwauth)
        query = GSQL_FP_RATIO_BWAUTH.format(bwauth, bwauth_sql, bwauth)
        logger.debug(f"{query}")
        target = G.SqlTarget(
            rawSql=query,
            format="table",
            datasource=config.datasource_pg,
        )
        panel = G.Histogram(
            title=f"Histogram Relays ratio {bwauth}",
            targets=[target],
            dataSource=config.datasource_pg,
            extraJson={
                "options": {"bucketSize": 0.05},
                "fieldConfig": {
                    "defaults": {"max": 9},
                }
            }
        )
        if len(panels) < 2:
            panels.append(panel)
        else:
            row = G.Row(panels=panels)
            rows.append(row)
            panels = [panel]
    row = G.Row(panels=panels)
    if row not in rows:
        rows.append(row)
    return rows


def dashboards_ratio_histogram():
    """Create grafana ratio histogram dashboard."""
    logger.info("Creating grafana Histogram Relays ratio dashboards.")
    rows = query_ratio_histogram_rows()
    title = "Histogram Relays ratio"
    uid = "histogram-ratio"
    ds = common.create_dashboard(title, uid, rows)
    return [ds]
