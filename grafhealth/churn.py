# SPDX-FileCopyrightText: 2023-2024 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

r"""Functions to create churn and retention dashboard.

Relays Churn Rate (RCR) is calculated as:
RCR = LR / PSR
LR: number of Lost Relays at the end of the period (taking into account
their concrete fingerprint)
PSR: Period Start number of Relays

The period is one hour (every consensus)

From https://ensa.fi/papers/sybilhunting-sec16.pdf
PSR = Ct-1
PER = Ct
LR = Ct-1\Ct
NR = Ct\Ct-1
RCR = LR / PSR = |Ct-1\Ct| / |Ct-1| = alfa_t (gone relays) = churn_lost
RRR = (PER - NR) / PSR = |Ct - Ct\Ct-1| / |Ct-1| != alfa_n
    = 1 - RCR = 1 - |Ct-1\Ct| / Ct-1 = |Ct - Ct\Ct-1| / Ct-1
alfa_n = churn_new = |Ct\Ct01| / Ct  (new relays)
"""

import logging

import grafanalib.core as G

from grafhealth import common

from . import config

logger = logging.getLogger(__name__)


GSQL_RCR_MEDIAN = """SELECT PERCENTILE_CONT(0.5)
WITHIN GROUP(ORDER BY churn_lost) as median_churn_lost
FROM relays_churn_rate
WHERE $__timeFilter(end_hour)"""
GSQL_RCRN_MEDIAN = """SELECT PERCENTILE_CONT(0.5)
WITHIN GROUP(ORDER BY churn_new) as median_churn_new
FROM relays_churn_rate_new
WHERE $__timeFilter(end_hour)"""
GSQL_RCR = """SELECT end_hour, -churn_lost AS churn_lost_negative
FROM relays_churn_rate
WHERE $__timeFilter(end_hour)
AND churn_lost < 1"""
GSQL_RCRN = """SELECT end_hour, churn_new
FROM relays_churn_rate_new
WHERE $__timeFilter(end_hour)"""
FLAGS = [
    "Guard",
    "Exit",
    "HSDir",
    "Stable",
    "Valid",
    "V2Dir",
]
GSQL_RCR_FLAG = """SELECT end_hour, -churn_lost AS churn_lost_negative
FROM relays_churn_rate_{}
WHERE $__timeFilter(end_hour)
AND churn_lost < 1"""
GSQL_RCRN_FLAG = """SELECT end_hour, churn_new
FROM relays_churn_rate_new_{}
WHERE $__timeFilter(end_hour)"""

GSQL_RCWCR_MEDIAN = """SELECT PERCENTILE_CONT(0.5)
WITHIN GROUP(ORDER BY cw_churn_lost) as median_cw_churn_lost
FROM relays_cw_churn_rate
WHERE $__timeFilter(end_hour)"""
GSQL_RCWCRN_MEDIAN = """SELECT PERCENTILE_CONT(0.5)
WITHIN GROUP(ORDER BY cw_churn_new) as median_cw_churn_new
FROM relays_cw_churn_rate_new
WHERE $__timeFilter(end_hour)"""
GSQL_RCWCR = """SELECT end_hour, -cw_churn_lost AS cw_churn_lost_negative
FROM relays_cw_churn_rate
WHERE $__timeFilter(end_hour)
AND cw_churn_lost < 1"""
GSQL_RCWCRN = """SELECT end_hour, cw_churn_new
FROM relays_cw_churn_rate_new
WHERE $__timeFilter(end_hour)"""
FLAGS = [
    "Guard",
    "Exit",
    "HSDir",
    "Stable",
    "Valid",
    "V2Dir",
]
GSQL_RCWCR_FLAG = """SELECT end_hour, -cw_churn_lost AS cw_churn_lost_negative
FROM relays_cw_churn_rate_{}
WHERE $__timeFilter(end_hour)
AND cw_churn_lost < 1"""
GSQL_RCWCRN_FLAG = """SELECT end_hour, cw_churn_new
FROM relays_cw_churn_rate_new_{}
WHERE $__timeFilter(end_hour)"""


def churn_rows():
    logger.info("Creating Churn rows.")
    rows = []
    panels = []
    targets = []
    target = G.SqlTarget(
        rawSql=GSQL_RCR_MEDIAN,
        format="table",
        datasource=config.datasource_pg,
    )
    panel = G.SingleStat(
        title="Relays Churn Rate (lost) Median",
        targets=[target],
        dataSource=config.datasource_pg,
    )
    panels.append(panel)
    target = G.SqlTarget(
        rawSql=GSQL_RCRN_MEDIAN,
        format="table",
        datasource=config.datasource_pg,
    )
    panel = G.SingleStat(
        title="Relays Churn Rate (new) Median",
        targets=[target],
        dataSource=config.datasource_pg,
    )
    panels.append(panel)
    row = G.Row(panels=panels)
    rows.append(row)
    panels = []
    targets = []
    target = G.SqlTarget(
        rawSql=GSQL_RCR,
        format="table",
        datasource=config.datasource_pg,
    )
    targets.append(target)
    target = G.SqlTarget(
        rawSql=GSQL_RCRN,
        format="table",
        datasource=config.datasource_pg,
    )
    targets.append(target)
    panel = G.TimeSeries(
        title="relays_churn_rate",
        targets=targets,
        dataSource=config.datasource_pg,
        spanNulls=True,
        showPoints="Always",
        fillOpacity=10,
    )
    panels.append(panel)
    row = G.Row(panels=panels)
    rows.append(row)
    for flag in FLAGS:
        targets = []
        target = G.SqlTarget(
            rawSql=GSQL_RCR_FLAG.format(flag),
            format="table",
            datasource=config.datasource_pg,
        )
        targets.append(target)
        target = G.SqlTarget(
            rawSql=GSQL_RCRN_FLAG.format(flag),
            format="table",
            datasource=config.datasource_pg,
        )
        targets.append(target)
        panel = G.TimeSeries(
            title=f"relays_churn_rate_{flag}",
            targets=targets,
            dataSource=config.datasource_pg,
            spanNulls=True,
            showPoints="Always",
            fillOpacity=10,
        )
        row = G.Row(panels=[panel])
        rows.append(row)
    return rows


def weight_churn_rows():
    logger.info("Creating Consensus Weight Churn rows.")
    rows = []
    panels = []
    targets = []
    target = G.SqlTarget(
        rawSql=GSQL_RCWCR_MEDIAN,
        format="table",
        datasource=config.datasource_pg,
    )
    panel = G.SingleStat(
        title="Relays Consensus Weight Churn Rate (lost) Median",
        targets=[target],
        dataSource=config.datasource_pg,
    )
    panels.append(panel)
    target = G.SqlTarget(
        rawSql=GSQL_RCWCRN_MEDIAN,
        format="table",
        datasource=config.datasource_pg,
    )
    panel = G.SingleStat(
        title="Relays Consensus Weight Churn Rate (new) Median",
        targets=[target],
        dataSource=config.datasource_pg,
    )
    panels.append(panel)
    row = G.Row(panels=panels)
    rows.append(row)
    panels = []
    targets = []
    target = G.SqlTarget(
        rawSql=GSQL_RCWCR,
        format="table",
        datasource=config.datasource_pg,
    )
    targets.append(target)
    target = G.SqlTarget(
        rawSql=GSQL_RCWCRN,
        format="table",
        datasource=config.datasource_pg,
    )
    targets.append(target)
    panel = G.TimeSeries(
        title="relays_cw_churn_rate",
        targets=targets,
        dataSource=config.datasource_pg,
        spanNulls=True,
        showPoints="Always",
        fillOpacity=10,
    )
    panels.append(panel)
    row = G.Row(panels=panels)
    rows.append(row)
    for flag in FLAGS:
        targets = []
        target = G.SqlTarget(
            rawSql=GSQL_RCWCR_FLAG.format(flag),
            format="table",
            datasource=config.datasource_pg,
        )
        targets.append(target)
        target = G.SqlTarget(
            rawSql=GSQL_RCWCRN_FLAG.format(flag),
            format="table",
            datasource=config.datasource_pg,
        )
        targets.append(target)
        panel = G.TimeSeries(
            title=f"relays_cw_churn_rate_{flag}",
            targets=targets,
            dataSource=config.datasource_pg,
            spanNulls=True,
            showPoints="Always",
            fillOpacity=10,
        )
        row = G.Row(panels=[panel])
        rows.append(row)
    return rows


def dashboards_churn():
    logger.info("Creating Churn dashboards.")
    dashboards = []
    rows = churn_rows()
    title = "Relays churn lost and new"
    uid = "churn_lost_new"
    ds = common.create_dashboard(title, uid, rows)
    dashboards.append(ds)
    rows = weight_churn_rows()
    title = ("Relays consensus weight churn lost and new",)
    uid = "cw_churn_lost_new"
    ds = common.create_dashboard(title, uid, rows)
    dashboards.append(ds)
    return dashboards
