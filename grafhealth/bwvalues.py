# SPDX-FileCopyrightText: 2023-2024 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

# For tpo/network-health/team#305:
# Find an easier way to verify whether a relay consensus weight makes sense
import logging

import grafanalib.core as G

from grafhealth import common

from . import config

logger = logging.getLogger(__name__)

GSQL_FP = """SELECT fingerprint
FROM bw_line"""
# WHERE $__timeFilter(time)"""
GSQL_SD_OBS_BW = """
SELECT published, bandwidth_observed
FROM server_descriptor WHERE fingerprint='${fingerprint}'
AND $__timeFilter(published)"""
GSQL_BW_VALUES_CALCULATED = """SELECT
    time,
    desc_bw_obs_last,
    weight_bytes,
    advertised_bw  * ratio AS calculated_weight_bytes,
    stream_bw,
    ratio * greatest(mu, muf) / NULLIF(desc_bw_obs_last, 0)
    AS calculated_stream_bw
FROM bw_line
INNER JOIN bandwidth_file
ON bw_line.bandwidth_file = bandwidth_file.digest
WHERE $__timeFilter(time)
AND fingerprint = '$fingerprint'"""
GSQL_BW_VALUES_CALCULATED_BWAUTH = """SELECT
    time,
    desc_bw_obs_last,
    weight_bytes,
    advertised_bw * ratio AS calculated_weight_bytes,
    stream_bw,
    ratio * greatest(mu, muf) / NULLIF(desc_bw_obs_last, 0)
    AS calculated_stream_bw
FROM bw_line
INNER JOIN bandwidth_file
ON bw_line.bandwidth_file = bandwidth_file.digest
WHERE $__timeFilter(time)
AND fingerprint = '$fingerprint'
{}"""
GSQL_RATIO_CALCULATED = """SELECT
    time,
    ratio,
    weight_bytes / NULLIF(desc_bw_obs_last, 0) as calculated_ratio
FROM bw_line
WHERE $__timeFilter(time)
AND fingerprint='$fingerprint'
AND desc_bw_obs_last > 0"""
GSQL_RATIO_CALCULATED_BWAUTH = """SELECT
    time,
    ratio,
    weight_bytes / NULLIF(desc_bw_obs_last, 0) as calculated_ratio
FROM bw_line
WHERE $__timeFilter(time)
AND fingerprint='$fingerprint'
AND desc_bw_obs_last > 0
{}"""


def server_desc_obs_bw_row():
    target = G.SqlTarget(
        rawSql=GSQL_SD_OBS_BW,
        format="table",
        datasource=config.datasource_pg,
    )
    panel = G.TimeSeries(
        title="Server descriptor observed bandwidth",
        targets=[target],
        dataSource=config.datasource_pg,
        unit="Bps",
    )
    panels = [panel]
    row = G.Row(panels=panels)
    return row


def bandiwdith_values_ratio_row():
    target = G.SqlTarget(
        rawSql=GSQL_BW_VALUES_CALCULATED,
        format="table",
        datasource=config.datasource_pg,
    )
    panel = G.TimeSeries(
        title="Bandwidth values",
        targets=[target],
        dataSource=config.datasource_pg,
        unit="Bps",
    )
    panels = [panel]
    target = G.SqlTarget(
        rawSql=GSQL_RATIO_CALCULATED,
        format="table",
        datasource=config.datasource_pg,
    )
    panel = G.TimeSeries(
        title="Ratio",
        targets=[target],
        dataSource=config.datasource_pg,
    )
    panels.append(panel)
    row = G.Row(panels=panels)
    return row


def bandwidth_values_ratio_bwauth_rows():
    rows = []
    for bwauth, bwauth_sql in common.FILTER_BWAUTHS.items():
        panels = []
        target = G.SqlTarget(
            rawSql=GSQL_BW_VALUES_CALCULATED_BWAUTH.format(bwauth_sql),
            format="table",
            datasource=config.datasource_pg,
        )
        panel = G.TimeSeries(
            title=f"Bandwidth values {bwauth}",
            targets=[target],
            dataSource=config.datasource_pg,
            unit="Bps",
        )
        panels.append(panel)
        target = G.SqlTarget(
            rawSql=GSQL_RATIO_CALCULATED_BWAUTH.format(bwauth_sql),
            format="table",
            datasource=config.datasource_pg,
        )
        panel = G.TimeSeries(
            title=f"Ratio {bwauth}",
            targets=[target],
            dataSource=config.datasource_pg,
        )
        panels.append(panel)
        row = G.Row(panels=panels)
        rows.append(row)
    return rows


def templating_vars_fp():
    templating = G.Templating(
        list=[
            G.Template(
                dataSource=config.datasource_pg,
                name="fingerprint",
                type="query",
                query=GSQL_FP,
                refresh=G.REFRESH_ON_TIME_RANGE_CHANGE,
            ),
        ]
    )
    return templating


def dashboards_bandwith_values():
    logger.info("Creating Relay bandwidth values dashboards.")
    templating = templating_vars_fp()
    rows = [server_desc_obs_bw_row()]
    rows.append(bandiwdith_values_ratio_row())
    rows.extend(bandwidth_values_ratio_bwauth_rows())
    title = "Relay bandwidth values, reported and calculated"
    uid = "bandwidth_values"
    ds = common.create_dashboard(title, uid, rows, templating=templating)
    return [ds]
