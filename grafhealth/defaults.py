# SPDX-FileCopyrightText: 2023-2024 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
"""Constants."""
import os.path

# paths
APP_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
CONFIG_PATH = os.path.join(APP_PATH, "config.toml")
GRAFANA_PATH = os.path.join(APP_PATH, "grafana")

# config.toml
GRAFANA_SECTION = "grafana"

# Grafana
GRAFANA_DATETIME_FMT = "%Y-%m-%d %H:%M:%S"

# SQL
DAYS_BACK = 30


LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "nocolor": {
            "format": "{asctime} {module}[{process}]: <{levelname}> "
            + "{filename}:{lineno} - {funcName} - {message}",
            "style": "{",
            "datefmt": "%b %d %H:%M:%S",
        },
        "color": {
            "()": "colorlog.ColoredFormatter",
            "log_colors": {
                "DEBUG": "green",
                "INFO": "blue",
                "WARNING": "bold_yellow",
                "ERROR": "bold_red",
                "CRITICAL": "bold_purple",
            },
            "format": "{log_color}{asctime} {module}[{process}]: "
            + "<{levelname}> {filename}:{lineno} - {funcName} - {message}",
            "style": "{",
            "datefmt": "%b %d %H:%M:%S",
        },
    },
    "handlers": {
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "color",
        },
        "file": {
            "level": "DEBUG",
            "class": "logging.handlers.TimedRotatingFileHandler",
            "filename": "debug.log",
            "when": "midnight",
            "utc": True,
            "backupCount": 84,  # 3 times 28 days
            "formatter": "nocolor",
        },
    },
    "loggers": {
        "": {
            "level": "DEBUG",
            "handlers": [
                "console",
            ],
            "propagate": True,
        },
    },
}
