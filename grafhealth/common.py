# SPDX-FileCopyrightText: 2023-2024 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

"""Common grafana functions and queries."""
import json
import logging

import grafanalib.core as G
import requests
from grafanalib._gen import DashboardEncoder

from . import config

logger = logging.getLogger(__name__)


SERIES_COLORS = {
    "weight_bytes": "green",
    "stream_bw": "yellow",
    "consensus_bandwidth": "light-green",
    "desc_bw_obs_last": "blue",
    "advertised_bw": "purple",
    "throughput": "orange",
    "bandwidth_observed": "light-blue",
    "network_weight": "semi-dark-green",
    "observed bw": "semi-dark-blue",
    "bw history": "red",
    "advertised bw": "semi-dark-purple",
}
GSQL_NODE_ID_FROM_FP = "SELECT concat('$', ${fingerprint})"
GSQL_NODES = "SELECT onionperf_node FROM onionperf_nodes"
GSQL_SCANNER_COUNTRY = """SELECT scanner_country FROM scanner_countries"""

FILTER_BWAUTHS = {
    "bastet": "AND bw_line.scanner_country = 'US' AND bw_line.software_version='1.8.1'",
    "moria1": "AND bw_line.scanner_country = 'US' AND bw_line.software_version='1.5.2'",
    "longclaw": "AND bw_line.dirauth_nickname = 'longclaw'",
    "gabelmoo": "AND bw_line.scanner_country = 'DE'",
    "maatuska": "AND bw_line.scanner_country = 'SE'",
    "tor26": "AND bw_line.scanner_country = 'AT'",
}

GSQL_SD_OBS_BW = """SELECT published, bandwidth_observed
FROM server_descriptor WHERE fingerprint=${fingerprint}
AND $__timeFilter(published)"""
GSQL_RATIO = """SELECT published,
    r_strm,
    r_strm_filt,
    ratio
FROM bw_line
WHERE $__timeFilter(published)
AND fingerprint = ${fingerprint}
AND scanner_country IN (${scanner_country})
AND ratio > 0"""
GSQL_RATIO_ONIONPERF = """SELECT unix_ts_start,
    ratio_unfilt,
    ratio_filt,
    ratio as onionperf_ratio
FROM relay_throughput_ratio
WHERE $__timeFilter(unix_ts_start)
AND fingerprint = ${fingerprint}
AND onionperf_node IN (${onionperf_node})"""

GSQL_BW_VALUES = """SELECT published,
  weight_bytes,
  stream_bw,
  consensus_bandwidth,
  desc_bw_obs_last,
  advertised_bw
FROM bw_line
WHERE fingerprint = ${fingerprint}
AND $__timeFilter(published)
AND scanner_country IN (${scanner_country})
AND ratio > 0"""
PROMQL_BW_HISTORY = """
(write_bandwidth_history{fingerprint='${fingerprint}'}
+ read_bandwidth_history{fingerprint='${fingerprint}'}) / 2.0"""
PROMQL_STREAM_BW = "bw_mean{fingerprint='${node_id}'}"
PROMQL_OBS_BW = "desc_bw_obs{fingerprint='${fingerprint}'}"
PROMQL_ADVERTISED_BW = """
min(desc_bw_obs{fingerprint='${fingerprint}'},
desc_bw_avg{fingerprint='${fingerprint}'})"""
PROMQL_WEIGHT_BYTES = "network_weight{fingerprint='${fingerprint}'} * 1000"
PROMQL_RATIO = """
max(r_strm{fingerprint='${node_id}'}, r_strm_filt{fingerprint='${node_id}'})
"""

GSQL_OTHER_DATA = """SELECT *
FROM relay_other_data('${__from:date:iso}', '${__to:date:iso}',
${fingerprint})"""
# Calculate a relay download throughput average, for time_interval=1week,
# for each relay as it appears in the Middle position
GSQL_THROUGHPUT_AVG = """SELECT  unix_ts_start,
  throughput
FROM relay_throughput_ratio
WHERE $__timeFilter(unix_ts_start)
AND fingerprint = ${fingerprint}
AND onionperf_node IN (${onionperf_node})"""
GSQL_NET_STREAM_BW = """SELECT published, mu, muf,
greatest(mu, muf) AS net_strm_avg
FROM bandwidth_file
WHERE $__timeFilter(published)
AND scanner_country = ANY(ARRAY[${scanner_country}])"""
GSQL_NET_THROUGHPUT = """SELECT start_week,
    net_throughput_unfilt,
    net_throughput_filt,
    net_throughput
FROM  net_throughput
WHERE $__timeFilter(start_week)
AND onionperf_node = ANY(ARRAY[${onionperf_node}])"""
GSQL_NET_RATIO = """SELECT DISTINCT
    published,
    avg(ratio) as net_ratio_avg
FROM bw_line
WHERE published BETWEEN '${__from:date:iso}' AND '${__to:date:iso}'
AND scanner_country = ${scanner_country}
GROUP BY published, scanner_country
"""
GSQL_NET_RATIO_ONIONPERF = """SELECT DISTINCT
    unix_ts_start,
    avg(ratio) AS net_ratio_onionperf_avg
FROM relay_throughput_ratio
WHERE unix_ts_start between '${__from:date:iso}' AND '${__to:date:iso}'
AND onionperf_node = ${onionperf_node}
GROUP BY unix_ts_start, onionperf_node;
"""


class Trend(G.Graph):
    """Grafana Trend visualization."""

    def to_json_data(self):
        panel_json = super().to_json_data()
        panel_json["type"] = "trend"
        return panel_json


def get_dashboards_json(
    dashboard,
    overwrite=False,
    message="Updated by grafanlib",
    folder_uid=None,
):
    """Generate JSON from Dashboard object with `dashboard` key."""
    return json.dumps(
        {
            "dashboard": dashboard.to_json_data(),
            "overwrite": overwrite,
            "message": message,
            "folderUid": folder_uid,
        },
        sort_keys=True,
        indent=2,
        cls=DashboardEncoder,
    )


def get_inside_dashboards_json(
    dashboard,
):
    """Generate JSON from Dashboard object without `dashboard` key."""
    return json.dumps(
        dashboard.to_json_data(),
        sort_keys=True,
        indent=2,
        cls=DashboardEncoder,
    )


def upload_to_grafana(dashboard_json_str, server, api_key, verify=True):
    """Upload dashboard to grafana server.

    :param json - dashboard json generated by grafanalib
    :param server - grafana server name
    :param api_key - grafana api key with read and write privileges
    """
    headers = {
        "Authorization": f"Bearer {api_key}",
        "Content-Type": "application/json",
    }
    logger.info("Uploading dashboard to grafana server.")
    response = requests.post(
        f"{server}/api/dashboards/db",
        data=dashboard_json_str,
        headers=headers,
        verify=verify,
        timeout=60,
    )
    logger.info(f"{response.status_code} - {response.content}")


def generate_timeseries_overrides():
    overrides = []
    for serie_name, color in SERIES_COLORS.items():
        item = {
            "matcher": {
                "id": "byName",
                "options": serie_name,
            },
            "properties": [
                {
                    "id": "color",
                    "value": {"fixedColor": color, "mode": "fixed"},
                },
            ],
        }
        overrides.append(item)
    return overrides


def templating_vars_fp_node_id_nick(query_fps):
    templating = G.Templating(
        list=[
            G.Template(
                dataSource=config.datasource_pg,
                name="fingerprint",
                type="query",
                query=query_fps,
                includeAll=True,
                multi=True,
                refresh=G.REFRESH_ON_TIME_RANGE_CHANGE,
            ),
            G.Template(
                dataSource=config.datasource_pg,
                name="node_id",
                type="query",
                query=GSQL_NODE_ID_FROM_FP,
                includeAll=True,
                multi=True,
            ),
            G.Template(
                dataSource=config.datasource_pg,
                name="onionperf_node",
                type="query",
                query=GSQL_NODES,
                default='op-de8a',
                includeAll=True,
                multi=True,
            ),
            G.Template(
                dataSource=config.datasource_pg,
                name="scanner_country",
                type="query",
                query=GSQL_SCANNER_COUNTRY,
                default='DE',
                includeAll=True,
                multi=True,
            ),
        ]
    )
    return templating


def net_stream_bw_avg_row():
    target = G.SqlTarget(
        rawSql=GSQL_NET_STREAM_BW,
        format="table",
        datasource=config.datasource_pg,
    )
    targets = [target]
    target = G.SqlTarget(
        rawSql=GSQL_NET_THROUGHPUT,
        format="table",
        datasource=config.datasource_pg,
    )
    targets.append(target)
    panel = G.TimeSeries(
        dataSource=config.datasource_pg,
        title="Network stream bandwidth / onionperf throughput average",
        targets=targets,
        unit="Bps",
        spanNulls=True,
        showPoints="Always",
        fillOpacity=10,
    )
    row = G.Row(
        # title="$fingerprint",
        # repeat="fingerprint",
        panels=[panel],
    )
    return row


def net_ratio_avg_row():
    target = G.SqlTarget(
        rawSql=GSQL_NET_RATIO,
        format="table",
        datasource=config.datasource_pg,
    )
    targets = [target]
    target = G.SqlTarget(
        rawSql=GSQL_NET_RATIO_ONIONPERF,
        format="table",
        datasource=config.datasource_pg,
    )
    targets.append(target)
    panel = G.TimeSeries(
        dataSource=config.datasource_pg,
        title="Network ratio average",
        targets=targets,
        spanNulls=True,
        showPoints="Always",
        fillOpacity=10,
    )
    row = G.Row(
        # title="$fingerprint",
        # repeat="fingerprint",
        panels=[panel],
    )
    return row

def bandwidth_values_inflation_rows():
    panels = []
    targets = []
    target = G.SqlTarget(
        rawSql=GSQL_BW_VALUES,
        format="table",
        datasource=config.datasource_pg,
    )
    targets.append(target)
    target = G.SqlTarget(
        rawSql=GSQL_THROUGHPUT_AVG,
        format="table",
        datasource=config.datasource_pg,
    )
    targets.append(target)
    target = G.SqlTarget(
        rawSql=GSQL_SD_OBS_BW,
        format="table",
        datasource=config.datasource_pg,
    )
    targets.append(target)
    target = G.Target(
        PROMQL_WEIGHT_BYTES,
        legendFormat="network_weight",
        datasource=config.datasource_vm,
    )
    targets.append(target)
    target = G.Target(
        PROMQL_OBS_BW,
        legendFormat="observed bw",
        datasource=config.datasource_vm,
    )
    targets.append(target)
    target = G.Target(
        PROMQL_STREAM_BW,
        legendFormat="stream bw",
        datasource=config.datasource_vm,
        # Because it is currently not possible to query by bwauth
        hide=True,
    )
    targets.append(target)
    target = G.Target(
        PROMQL_BW_HISTORY,
        legendFormat="bw history",
        datasource=config.datasource_vm,
    )
    targets.append(target)
    target = G.Target(
        PROMQL_ADVERTISED_BW,
        legendFormat="advertised bw",
        datasource=config.datasource_vm,
    )
    targets.append(target)
    panel = G.TimeSeries(
        dataSource="-- Mixed --",
        title="Bandwidth values",
        targets=targets,
        unit="Bps",
        spanNulls=True,
        showPoints="Always",
        fillOpacity=10,
        overrides=generate_timeseries_overrides(),
    )
    panels.append(panel)
    targets = []
    target = G.SqlTarget(
        rawSql=GSQL_RATIO,
        format="table",
        datasource=config.datasource_pg,
    )
    targets.append(target)
    target = G.Target(
        PROMQL_RATIO,
        legendFormat="ratio (VM)",
        datasource=config.datasource_vm,
        # Because it is currently not possible to query by bwauth
        hide=True,
    )
    targets.append(target)
    target = G.SqlTarget(
        rawSql=GSQL_RATIO_ONIONPERF,
        format="table",
        datasource=config.datasource_pg,
    )
    targets.append(target)
    panel = G.TimeSeries(
        dataSource="-- Mixed --",
        title="Ratio",
        targets=targets,
    )
    panels.append(panel)
    targets = []
    target = G.SqlTarget(
        rawSql=GSQL_OTHER_DATA,
        format="table",
        datasource=config.datasource_pg,
    )
    targets.append(target)
    panel = G.Table(
        dataSource=config.datasource_pg,
        title="Other data",
        targets=targets,
    )
    panels.append(panel)
    row = G.Row(
        title="$fingerprint",
        repeat="fingerprint",
        panels=panels,
    )
    return [row]


def create_dashboard(
    title,
    uid,
    rows,
    description="",
    templating=G.Templating(),
    provisioned=False,
):
    ds = G.Dashboard(
        title=title,
        uid=uid,
        rows=rows,
        time=G.Time(*config.date_interval),
        description=description,
        templating=templating,
        refresh="1d",
        tags=["provisioned"] if provisioned else [],
    ).auto_panel_ids()
    return ds
