# SPDX-FileCopyrightText: 2023-2024 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

"""Micelanous useful functions."""
import argparse
import datetime
import logging

from grafhealth import defaults

logger = logging.getLogger(__name__)


def valid_datetime(s):
    try:
        return datetime.datetime.fromisoformat(s)
    except ValueError as exc:
        msg = "Not a valid date: '{s}'."
        raise argparse.ArgumentTypeError(msg) from exc


def days_back2datetime(days_back=defaults.DAYS_BACK):
    now = datetime.datetime.utcnow()
    delta = datetime.timedelta(days=days_back)
    start_dt = now - delta
    return (
        start_dt,
        now,
    )


def days_back2grafana_time(days_back=defaults.DAYS_BACK):
    start_dt, now = days_back2datetime(days_back)
    return (
        start_dt.strftime(defaults.GRAFANA_DATETIME_FMT),
        now.strftime(defaults.GRAFANA_DATETIME_FMT),
    )
