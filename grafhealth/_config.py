# SPDX-FileCopyrightText: 2023-2024 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

"""Parse configuration file."""
import logging
import sys

from grafhealth import defaults, util

logger = logging.getLogger(__name__)


class Config:
    def __init__(self, config_path=defaults.CONFIG_PATH):
        self.api_key = None
        self.server = None
        self.datasource_pg = None
        self.datasource_vm = None
        self.folder_uid = None
        self.provisioned = False
        self.date_interval = util.days_back2grafana_time(defaults.DAYS_BACK)
        # self.args = None
        config_dict = self.parse_config_file(config_path)
        grafana_config = config_dict.get(defaults.GRAFANA_SECTION, None)
        if not grafana_config:
            logger.critical(
                "Configuration %s missing `[grafana]` section.", config_path
            )
            sys.exit(1)
        for key in [
            "api_key",
            "server",
            "datasource_pg",
            "datasource_vm",
            "folder_uid",
        ]:
            setattr(self, key, grafana_config.get(key, None))
        # return self

    def parse_config_file(self, config_path=defaults.CONFIG_PATH):
        import toml

        logger.info("Parsing %s.", config_path)
        try:
            parsed_obj = toml.load(config_path)
        except (TypeError, toml.TomlDecodeError) as e:
            logger.critical("Failed to parse configuration: %s", e)
            sys.exit(1)
        return parsed_obj

    def set_date_interval(self, date_interval=None):
        if date_interval:
            self.date_interval = date_interval

    def set_provisioned(self, provisioned=False):
        self.provisioned = provisioned
