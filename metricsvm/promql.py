# SPDX-FileCopyrightText: 2023-2024 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

"""PromQL queries."""

query_history = (
    "write_bandwidth_history{{fingerprint='{}'}} * 10 "
    "+ read_bandwidth_history{{fingerprint='{}'}} * 10"
)
query_advertised = (
    "min(desc_bw_obs{{fingerprint='{}'}}, "
    + "desc_bw_avg{{fingerprint='{}'}})"
)
query_bw_mean = "bw_mean{{fingerprint='${}'}}"

# percentile = (float(n*i)/len(path_list))*100
# bottomk(0.5, max(r_strm, r_strm_filt) by (fingerprint))
