TODO
====

- [X] Create low consensus dashboard
- [X] Create CDF dashboard
- [X] Add config.toml and parse it
- [X] Refactor/Create main module to create the different dashboards
  - tpo/network-health/team#313: Inflation detection
  - tpo/network-health/metrics/monitoring-and-alerting#29: CDF
  - tpo/network-health/sbws#40190 Low consensus bandwidth
- [X] Upload all dashboards
- [X] Save all dashboards locally
- [X] Inflation detection: create dashboard with bandwidth histories for the
  relays with lower ratios without localdb/transformation
- [X] Add util functions with date conversions
- [X] Add setup.cfg and other meta files
- [X] Inflation detection: Create flags panel without localdb/transformation
- [X] Inflation detection: Create functions to detect possible inflated relays
- [X] Inflation detection: sum read and write histories to compare with
   observed bandwidth
- [X] Upload dashboard to folder
- [ ] Inflation detection: Obtain overloaded?
- [ ] Inflation detection: Obtain lower ratio percentil insted or apart of
  obtaining relays with lower ratio?
- [ ] Inflation detection: Calculate percent difference directly with SQL,
- instead of calculating it with python
- [X] CDF: add missing panels
- [X] low consensus: add missing panels
- [X] Add tpo/network-health/team#305 dashboard (Find an easier way to verify
  whether a relay consensus weight makes sense dashboard)
- [X] Add tpo/network-health/metrics/monitoring-and-alerting#30 dashboard (
  stream bandwidth by relay weight)
- [ ] Add alerts for tpo/network-health/metrics/monitoring-and-alerting#29
