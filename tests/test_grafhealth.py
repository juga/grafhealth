# SPDX-FileCopyrightText: 2023-2024 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
from grafhealth.grafhealth import Grafhealth, create_parser


def test_grafhealth():
    """Test that all grafana dashboards are created.

    Still doesn't check that they are correct.
    """
    grafhealth = Grafhealth()
    parser = create_parser()
    args = parser.parse_args(["-a", "-p", "-w"])
    grafhealth.set_args(args)
    grafhealth.set_config_date_interval()
    grafhealth.set_config_provisioned()
    grafhealth.create_dashboards()
    grafhealth.write_dashboards_json()
    grafhealth.upload_dashboards_grafana_server()
