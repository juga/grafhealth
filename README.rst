grafhealth
==========

WIP tool to generate grafana dashboards for some issues in network-health.

Installation
------------

From source: it's recommended to create a virtualenv_, to avoid having
different versions of the same libraries in your system.

To create a ``virtualenv``::

    virtualenv venv -p /usr/bin/python3
    source venv/bin/activate

Clone ``grafhealth``::

    git clone https://gitlab.torproject.org/juga/grafhealth

Install the python dependencies::

    cd grafhealth && pip install -e .  # To install it for development

.. _virtualenv: https://virtualenv.pypa.io/en/latest/installation.html

Configuration
-------------

Copy ``config.example.toml`` to ``config.toml`` and replace the variables.
