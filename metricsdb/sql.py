# SPDX-FileCopyrightText: 2023-2024 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause
"""SQL queries to run in PostreSQL or grafana."""

# Number of relays with lowest ratio to check for inflation
LOWEST_RATIO_NUMBER_RELAYS = 30

# For tpo/network-health/team#313: Inflation detection
# ----------------------------------------------------

SELECT_DISTINCT_RATIO = """
    SELECT DISTINCT
        node_id,
        AVG(GREATEST(r_strm, r_strm_filt)) AS ratio
    FROM bandwidth_record"""
SELECT_DISTINCT_RATIO_BWAUTH = """
    SELECT DISTINCT
        node_id,
        AVG(GREATEST(r_strm, r_strm_filt)) AS ratio_{}
    FROM bandwidth_record"""
INNER_JOIN_BANDWIDTH_FILE = """
    INNER JOIN bandwidth_file
    ON bandwidth_record.bandwidth_file = bandwidth_file.digest"""
WHERE_TIME = "WHERE time BETWEEN '{}' AND '{}'"
WHERE_PUBLISHED = "WHERE published BETWEEN '{}' AND '{}'"
GWHERE_TIME = "WHERE $__timeFilter(time)"
FILTER_AND_RATIO = "AND GREATEST(r_strm, r_strm_filt) > 0"
GROUP_BY_NODE_ID = "GROUP BY node_id"
ORDER_BY_RATIO = "ORDER BY ratio"
ORDER_BY_RATIO_BWAUTH = "ORDER BY ratio_{}"
LIMIT_NUMBER_RELAYS = "LIMIT {}".format(LOWEST_RATIO_NUMBER_RELAYS)
SELECT_DISTINCT_SUBSTRING_SUBQUERY = """
    SELECT
        substring(node_id,2) AS fingerprint
    FROM ({}) AS subquery
    """

QUERY_LOWEST_RATIO = "{} {} {} {} {}".format(
    SELECT_DISTINCT_RATIO,
    WHERE_TIME,
    # FILTER_AND_RATIO,
    GROUP_BY_NODE_ID,
    ORDER_BY_RATIO,
    LIMIT_NUMBER_RELAYS,
)

GQUERY_LOWEST_RATIO = "{} {} {} {} {}".format(
    SELECT_DISTINCT_RATIO,
    GWHERE_TIME,
    # FILTER_AND_RATIO,
    GROUP_BY_NODE_ID,
    ORDER_BY_RATIO,
    LIMIT_NUMBER_RELAYS,
)
SELECT_DISTINCT_RATIO_FLAGS = """
    SELECT DISTINCT
        substring(node_id,2) AS fingerprint,
        AVG(GREATEST(r_strm, r_strm_filt)) AS ratio,
        flags
    FROM bandwidth_record
"""
INNER_JOIN_NETWORK_STATUS = """
    INNER JOIN network_status_entry
    ON fingerprint = network_status_entry.fingerprint
    """
GWHERE_BANDWIDTH_RECORD_TIME = "WHERE $__timeFilter(bandwidth_record.time)"
GROUP_BY_FINGERPRINT = "GROUP BY fingerprint"
GQUERY_LOWEST_RATIO_FLAGS = "{} {} {} {} {} {}".format(
    SELECT_DISTINCT_RATIO_FLAGS,
    INNER_JOIN_NETWORK_STATUS,
    GWHERE_TIME,
    # FILTER_AND_RATIO,
    GROUP_BY_FINGERPRINT,
    ORDER_BY_RATIO,
    LIMIT_NUMBER_RELAYS,
)
GROUP_BY_NODE_ID_NICK = "GROUP BY node_id, nick"

SELECT_DISTINCT_RATIO_NICK = """
    SELECT DISTINCT
        node_id,
        nick,
        AVG(GREATEST(r_strm, r_strm_filt)) AS ratio
    FROM bandwidth_record"""

QUERY_LOWEST_RATIO_NICK = "{} {} {} {} {}".format(
    SELECT_DISTINCT_RATIO,
    GWHERE_TIME,
    # FILTER_AND_RATIO,
    GROUP_BY_NODE_ID,
    ORDER_BY_RATIO,
    LIMIT_NUMBER_RELAYS,
)
GQUERY_LOWEST_RATIO_FLAGS = """
    SELECT
    fingerprint,
    AVG(GREATEST(r_strm, r_strm_filt)) as ratio,
    flags
    FROM network_status_entry
    INNER JOIN bandwidth_record
    ON substring(bandwidth_record.node_id,2)=network_status_entry.fingerprint
    WHERE
    $__timeFilter(bandwidth_record.time)
    GROUP BY fingerprint, flags
    ORDER BY ratio LIMIT 30
"""
# AND GREATEST(r_strm, r_strm_filt) > 0
GQUERY_LOWEST_RATIO_FLAGS = """
    SELECT DISTINCT
        substring(node_id,2) AS fingerprint,
        AVG(GREATEST(r_strm, r_strm_filt)) AS ratio,
        flags
    FROM bandwidth_record

    INNER JOIN network_status_entry
    ON fingerprint = network_status_entry.fingerprint
     WHERE $__timeFilter(bandwidth_record.time)
     GROUP BY node_id, flags ORDER BY ratio LIMIT 30
     """
#  AND GREATEST(r_strm, r_strm_filt) > 0

# QUERY_WEIGHT_LOWEST_RATIO = """
#     SELECT bandwidth_weight
#     FROM network_status_entry
#     WHERE $__timeFilter(time)
#     AND fingerprint IN (
#     SELECT substring(node_id,2) AS fingerprint
#         from (
#         SELECT DISTINCT
#             node_id,
#             AVG(GREATEST(r_strm, r_strm_filt)) AS ratio
#         FROM bandwidth_record
#         WHERE $__timeFilter(time)
#         AND GREATEST(r_strm, r_strm_filt) > 0
#         GROUP BY node_id ORDER BY ratio LIMIT 30
#     ) as subquery
#     )
# """
QUERY_WEIGHT_LOWEST_RATIO = """
    SELECT bandwidth_weight
    FROM network_status_entry
    WHERE time BETWEEN '{}' AND '{}'
    AND concat('$', fingerprint) IN ({})
"""
# QUERY_HIGHEST_WEIGHT_LOWEST_RATIO = """
#     SELECT DISTINCT
#         node_id,
#         AVG(GREATEST(r_strm, r_strm_filt)) AS ratio,
#         AVG(bw) as weight
#     FROM bandwidth_record
#     WHERE $__timeFilter(time)
#     AND GREATEST(r_strm, r_strm_filt) > 0
#     GROUP BY node_id ORDER BY ratio, weight DESC LIMIT 10
# """
QUERY_HIGHEST_WEIGHT_LOWEST_RATIO = """
    SELECT
        node_id,
        AVG(GREATEST(r_strm, r_strm_filt)) AS ratio,
        AVG(bw) as weight,
        flags
    FROM bandwidth_record
    WHERE time BETWEEN '{}' AND '{}'
    AND node_id IN ({}}
    GROUP BY node_id
    order by ratio, weight desc
"""
QUERY_NODE_ID_NICKNAME_FLAGS = """
    SELECT
        substring(node_id,2) AS fingerprint,
        nickname,
        MAX(GREATEST(r_strm, r_strm_filt)) AS ratio,
        MAX(bw) as weight,
        flags
    FROM bandwidth_record
    INNER JOIN network_status_entry
    ON fingerprint = network_status_entry.fingerprint
    WHERE $__timeFilter(bandwidth_record.time)
    AND fingerprint IN {}
    order by ratio, weight desc
"""

QUERY_INFLATION = """
    SELECT
        substring(node_id,2) AS fingerprint,
        nick,
        MAX(GREATEST(r_strm, r_strm_filt)) AS max_ratio,
        MAX(bw) as max_weight
    FROM bandwidth_record
    WHERE $__timeFilter(time)
    AND substring(node_id,2) IN {}
    GROUP BY node_id, nick
"""
QUERY_INFLATION2 = """
    SELECT
        fingerprint,
        flags
    FROM network_status_entry
    WHERE $__timeFilter(time)
    AND fingerprint IN {}
    GROUP BY fingerprint, flags
"""

# QUERY_NODE_ID = """
#     SELECT DISTINCT
#         node_id
#     FROM bandwidth_record
#     WHERE $__timeFilter(time)
#     AND GREATEST(r_strm, r_strm_filt) > 0
#     GROUP BY node_id
#     ORDER BY AVG(GREATEST(r_strm, r_strm_filt)), AVG(bw) DESC
#     LIMIT 10
# """
# QUERY_NODE_ID = """
#     SELECT substring(node_id,2) AS fingerprint
#         from (
#     SELECT DISTINCT
#         node_id,
#         AVG(GREATEST(r_strm, r_strm_filt)) AS ratio,
#         AVG(bw) as weight
#     FROM bandwidth_record
#     WHERE time BETWEEN '2024-01-24' AND '2024-01-26'
#     AND GREATEST(r_strm, r_strm_filt) > 0
#     GROUP BY node_id
#     ORDER BY ratio, weight DESC
#     LIMIT 10
#     ) as subquery
# """
QUERY_NODE_ID_DATETIME = """
    SELECT node_id
        from (
    SELECT DISTINCT
        node_id,
        AVG(GREATEST(r_strm, r_strm_filt)) AS ratio,
        AVG(bw) as weight
    FROM bandwidth_record
    WHERE time BETWEEN '{}' AND '{}'
    AND GREATEST(r_strm, r_strm_filt) > 0
    GROUP BY node_id
    ORDER BY ratio, weight DESC
    LIMIT 10
    ) as subquery
"""

QUERY_FLAGS = """
    SELECT flags
    from network_status_entry
    WHERE concat('$', fingerprint) in ($node_id_1w )
"""

INFLATION_DATETIME_INTERVALS = ["7d", "14d", "28d"]

QUERY_HIGHEST_RATIO = "{} {} {} {} DESC {}".format(
    SELECT_DISTINCT_RATIO,
    WHERE_TIME,
    # FILTER_AND_RATIO,
    GROUP_BY_NODE_ID,
    ORDER_BY_RATIO,
    LIMIT_NUMBER_RELAYS,
)

QUERY_FLAGS = """SELECT DISTINCT
        fingerprint,
        CASE WHEN
            flags LIKE '%Guard%'
            AND flags NOT LIKE '%Exit%'
            AND flags NOT LIKE '%Stable%'
            THEN 1
            ELSE 0
        END
        AS only_guard,

        CASE WHEN
            flags NOT LIKE '%Guard%'
            AND flags LIKE '%Exit%'
            AND flags NOT LIKE '%Stable%'
            THEN 1
            ELSE 0
        END
        AS only_exit,

        CASE WHEN
            flags NOT LIKE '%Guard%'
            AND flags NOT LIKE '%Exit%'
            AND flags LIKE '%Stable%'
            THEN 1
            ELSE 0
        END
        AS only_stable,

        CASE WHEN
            flags LIKE '%Guard%'
            AND flags NOT LIKE '%Exit%'
            AND flags LIKE '%Stable%'
            THEN 1
            ELSE 0
        END
        AS stable_guard,

        CASE WHEN
            flags NOT LIKE '%Guard%'
            AND flags LIKE '%Exit%'
            AND flags LIKE '%Stable%'
            THEN 1
            ELSE 0
        END
        AS stable_exit,

        CASE WHEN
            flags LIKE '%Guard%'
            AND flags LIKE '%Exit%'
            AND flags NOT LIKE '%Stable%'
            THEN 1
            ELSE 0
        END
        AS exit_guard,

        CASE WHEN
            flags LIKE '%Guard%'
            AND flags LIKE '%Exit%'
            AND flags LIKE '%Stable%'
            THEN 1
            ELSE 0
        END
        AS stable_exit_guard
    FROM network_status_entry
    WHERE fingerprint IN ({})
"""

QUERY_RELAY_LAST_FLAGS = """
    SELECT flags
    FROM network_status_entry
    {}
        AND fingerprint='{}'
    ORDER BY time DESC
    LIMIT 1;
"""

QUERY_RELAY_LAST_ASN = """
    SELECT autonomous_system
    FROM server_status
    {}
        AND fingerprint='{}'
    ORDER BY published DESC
    LIMIT 1;
"""

# For tpo/network-health/sbws#40190:
# Low consensus weight and measurements for relays with high capacity
# -------------------------------------------------------------------

SELECT_STRM_BW_BY_RATIO = """
    SELECT
        time,
        node_id,
        bw_mean / GREATEST(r_strm, r_strm_filt) AS bw_mean_ratio
    FROM bandwidth_record
"""
QUERY_STRM_BW_BY_RATIO_BWAUTH = "{} {} {} {} {} {}".format(
    SELECT_STRM_BW_BY_RATIO,
    INNER_JOIN_BANDWIDTH_FILE,
    GWHERE_TIME,
    "AND node_id='{}'",
    {},
    "ORDER BY time",
)

# For tpo/network-health/monitoring-and-alerting#29: CDF
# ------------------------------------------------------

# The following 3 queries should be equivalent


SELECT_WEIGHT_BY_OBSERVED = """
    SELECT
        bandwidth_record.bw * 1000 / bandwidth_record.desc_bw_obs_mean
            AS ratio,
        cume_dist() over (
            ORDER BY
            bandwidth_record.bw * 1000 / bandwidth_record.desc_bw_obs_last
        ) AS cdf_{}
    FROM bandwidth_record
    """
FILTER_AND_OBS = "AND desc_bw_obs_last > 0"
QUERY_WEIGHT_BY_OBSERVED_BWAUTH = "{} {} {} {} {}".format(
    SELECT_WEIGHT_BY_OBSERVED,
    INNER_JOIN_BANDWIDTH_FILE,
    GWHERE_TIME,
    FILTER_AND_OBS,
    {},
    ORDER_BY_RATIO,
)

SELECT_STREAM_BY_NET_AVG = """
    SELECT
        bw_mean / GREATEST(mu, muf) AS ratio,
        cume_dist() over (
            ORDER BY bw_mean / GREATEST(mu, muf)
        ) AS cdf_{}
    FROM bandwidth_record
    """
QUERY_STREAM_BY_NET_AVG_BWAUTH = "{} {} {} {} {}".format(
    SELECT_STREAM_BY_NET_AVG,
    INNER_JOIN_BANDWIDTH_FILE,
    GWHERE_TIME,
    {},
    ORDER_BY_RATIO,
)

# For tpo/network-health/metrics/monitoring-and-alerting#30:
# Create panels for stream bandwidth by relay weight
# ----------------------------------------------------------

SELECT_DISTINCT_NET_STREAM_BY_OBSERVED = """
SELECT DISTINCT
    node_id,
    AVG(GREATEST(mu, muf)) / AVG(desc_bw_obs_last) AS net_avg_by_observed_{}
FROM
  bandwidth_record
"""
QUERY_NET_STREAM_BY_OBSERVED = "{} {} {} {} {} {}".format(
    SELECT_DISTINCT_NET_STREAM_BY_OBSERVED,
    INNER_JOIN_BANDWIDTH_FILE,
    GWHERE_TIME,
    {},
    FILTER_AND_OBS,
    GROUP_BY_NODE_ID,
)

SELECT_DISTINCT_STREAM_BY_WEIGHT = """
SELECT DISTINCT
    node_id,
    AVG(bw_mean) / AVG(bw * 1000) AS strm_by_w_{}
FROM bandwidth_record
"""
QUERY_STREAM_BY_WEIGHT = "{} {} {} {} {}".format(
    SELECT_DISTINCT_STREAM_BY_WEIGHT,
    INNER_JOIN_BANDWIDTH_FILE,
    GWHERE_TIME,
    {},
    GROUP_BY_NODE_ID,
)

SELECT_DISTINCT_WEIGHT_BY_ADVERTISED = """
SELECT DISTINCT
    node_id,
    AVG(bw * 1000) / AVG(LEAST(desc_bw_obs_last, desc_bw_avg, 10000000))
    AS weight_by_advertised_{}
FROM
  bandwidth_record
"""
QUERY_WEIGHT_BY_ADVERTISED = "{} {} {} {} {} {}".format(
    SELECT_DISTINCT_WEIGHT_BY_ADVERTISED,
    INNER_JOIN_BANDWIDTH_FILE,
    GWHERE_TIME,
    {},
    FILTER_AND_OBS,
    GROUP_BY_NODE_ID,
)

# For tpo/network-health/team#305:
# Find an easier way to verify whether a relay consensus weight makes sense
# -------------------------------------------------------------------------
SELECT_BW_VALUES = """
    SELECT
        time,
        desc_bw_obs_last,
        bw * 1000 AS weight_KB,
        least(desc_bw_obs_last, desc_bw_avg, desc_bw_bur)
        * greatest(r_strm, r_strm_filt) AS calculated_weight,
        bw_mean,
        greatest(r_strm, r_strm_filt) * greatest(mu, muf) / desc_bw_obs_mean
        AS calculated_bw_mean
    FROM bandwidth_record
"""
GFILTER_AND_NODE_ID = "AND node_id = '$node_id'"
QUERY_BW_VALUES = "{} {} {} {} {}".format(
    SELECT_BW_VALUES,
    INNER_JOIN_BANDWIDTH_FILE,
    GWHERE_TIME,
    {},
    GFILTER_AND_NODE_ID,
    # FILTER_AND_RATIO,
)
SELECT_DISTINCT_NODE_ID = """SELECT DISTINCT node_id
    FROM bandwidth_record
"""
QUERY_NODE_ID = "{} {}".format(
    SELECT_DISTINCT_NODE_ID,
    GWHERE_TIME,
)

# For tpo/network-health/analysis#19: stream bandwidth by relay weight
# --------------------------------------------------------------------
QUERY_STREAM_BW = """
    SELECT k, percentile_disc(k)
    within group (order by bw_mean)
    from bandwidth_record, generate_series(0.03, 1, 0.03) as k
    group by k
"""

QUERY_STREAM_BW = """
SELECT bw_mean, ntile(100) over (order by bw_mean)
from bandwidth_record
"""

QUERY_STREAM_BW = """
SELECT max(buckets), ntile as percentile
from
  (SELECT bw_mean, ntile(100) over (order by bw_mean)
  from bandwidth_record) as buckets
group by 2 order by 2
"""

QUERY_STREAM_BW = """
SELECT bw_mean, ntile(100) over (order by bw_mean)
from bandwidth_record
WHERE bw_mean > 0
AND $__timeFilter(time)
"""
