# SPDX-FileCopyrightText: 2023-2024 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

"""Connect to PostgreSQL database."""
import logging
import sys

import psycopg2

from grafhealth import util

logger = logging.getLogger(__name__)


def connect(config_pg):
    """Connect to the PostgreSQL database server."""
    conn = None
    try:
        conn_str = config_pg.get("conn_str", None)
        if not conn_str:
            logger.error("No connection string")
            sys.exit(1)
        logger.debug("Connecting to DB %s.", conn_str)
        conn = psycopg2.connect(conn_str)
        conn.autocommit = True
        cur = conn.cursor()
    except (Exception, psycopg2.DatabaseError) as error:
        logger.critical(error)
        if conn:
            conn.close()
        sys.exit(1)
    logger.info("Connected to the DB.")
    return cur


def fetch_rows(cur, query, date_interval=util.days_back2grafana_time()):
    query = query.format(*date_interval)
    logger.debug(f"{query}")
    cur.execute(query)
    rows = cur.fetchall()
    logger.info("Fetched %s rows.", cur.rowcount)
    return rows
