# SPDX-FileCopyrightText: 2023-2024 The Tor Project, Inc.
#
# SPDX-License-Identifier: BSD-3-Clause

"""Functions to run PostgreSQL queries."""
import datetime
import logging

from grafhealth import sql, util

from . import config

logger = logging.getLogger(__name__)


# For tpo/network-health/team#313: Inflation detection
# ----------------------------------------------------


def fetch_relays_lowest_ratio(cur):
    """
    Fetch relays with lowest ratio.

    Example:

    SELECT
        substring(node_id,2) AS fingerprint
    FROM (
        SELECT DISTINCT
            node_id,
            AVG(GREATEST(r_strm, r_strm_filt)) AS ratio
        FROM bandwidth_record
        WHERE
            time BETWEEN '2024-01-20 09:48:22' AND '2024-01-22 09:48:22'
            AND GREATEST(r_strm, r_strm_filt) > 0
            GROUP BY node_id
            ORDER BY ratio LIMIT 30
    ) AS subquery
    """
    # To know which is the maximum ratio
    subquery = sql.GSQL_LOWEST_RATIO.format(*config.date_interval)
    cur.execute(subquery)
    rows = cur.fetchall()
    if rows:
        ratio_thresold = rows[-1][1]
        logger.info(f"{ratio_thresold}")
    query = sql.SELECT_DISTINCT_SUBSTRING_SUBQUERY.format(subquery)
    logger.debug(f"{query}")
    cur.execute(query)
    rows = cur.fetchall()
    logger.info("Fetched %s relays.", cur.rowcount)
    relay_fps = list(map(lambda x: x[0], rows))
    return relay_fps


def fetch_relays_highest_ratio(
    cur,
):
    """Fetch relays with highest ratio.

    For comparison with lowest ratio.
    """
    subquery = sql.SQL_HIGHEST_RATIO.format(*config.date_interval)
    cur.execute(subquery)
    rows = cur.fetchall()
    if rows:
        ratio_thresold = rows[-1][1]
        logger.info(f"{ratio_thresold}")
    query = sql.SELECT_DISTINCT_SUBSTRING_SUBQUERY.format(subquery)
    logger.debug(f"{query}")
    cur.execute(query)
    rows = cur.fetchall()
    logger.info("Fetched %s relays.", cur.rowcount)
    relay_fps = list(map(lambda x: x[0], rows))
    return relay_fps


def fetch_relay_flags(cur, relay_fp):
    """
    Fetch relays' flags.

    Example:

    SELECT flags
    FROM network_status_entry
    WHERE time BETWEEN '2024-01-20 11:06:40' AND '2024-01-22 11:06:40'
        AND fingerprint='EDD0433E613269C2BE6C79C07DE710A87F1F74AA'
    ORDER BY time DESC
    LIMIT 1;
    """
    query = sql.GSQL_RELAY_LAST_FLAGS.format(
        sql.WHERE_TIME.format(*config.date_interval), relay_fp
    )
    logger.debug(f"{query}")
    cur.execute(query)
    row = cur.fetchone()
    if row:
        return row[0]
    return ""


def fetch_relay_asn(cur, relay_fp):
    """
    Fetch relays' autonomous server.

    Example:

    SELECT autonomous_system
    FROM server_status
    WHERE published BETWEEN '2024-01-20 11:06:40' AND '2024-01-22 11:06:40'
        AND fingerprint='EDD0433E613269C2BE6C79C07DE710A87F1F74AA'
    ORDER BY published DESC
    LIMIT 1;
    """
    query = sql.GSQL_RELAY_LAST_ASN.format(
        sql.WHERE_PUBLISHED.format(*config.date_interval), relay_fp
    )
    logger.debug(f"{query}")
    cur.execute(query)
    row = cur.fetchone()
    return row[0]


def datetime_intervals_inflation():
    """
    Example:

    [(datetime.datetime(2024, 2, 9, 17, 38, 51, 252471),
     datetime.datetime(2024, 2, 15, 17, 38, 51, 252471)),

    """
    # 4 weeks
    intervals = [(i, i + 6) for i in range(0, 56, 6)]
    dates = []
    now = datetime.datetime.utcnow()
    for days_ago_end, days_ago_start in intervals:
        start = now - datetime.timedelta(days_ago_start)
        end = now - datetime.timedelta(days_ago_end)
        dates.append((start, end))
    return dates


def fetch_relays_lowest_ratio_highest_weight_intervals(cur):
    dt_intervals = util.datetime_intervals_inflation()
    subquery = """
    SELECT node_id FROM
    (SELECT DISTINCT node_id,
        AVG(GREATEST(r_strm, r_strm_filt)) AS ratio,
        AVG(bw) as weight
    FROM bandwidth_record
    WHERE time between '{}' AND '{}'
    AND GREATEST(r_strm, r_strm_filt) > 0
    GROUP BY node_id
    ORDER BY ratio, weight DESC
    LIMIT 30) as subquery
    """
    query = " intersect ".join([subquery] * 8).format(
        *list(sum(dt_intervals, ()))
    )
    cur.execute(query)
    rows = cur.fetchall()
    logger.info("Fetched %s relays.", cur.rowcount)
    relay_fps = list(map(lambda x: x[0], rows))
    return relay_fps


def fetch_relays_highest_weight_lowest_ratio_intervals(cur):
    dt_intervals = util.datetime_intervals_inflation()
    logger.debug(f"{dt_intervals}")
    subquery = """
    SELECT node_id FROM
    (SELECT DISTINCT node_id,
        AVG(GREATEST(r_strm, r_strm_filt)) AS ratio,
        AVG(bw) as weight
    FROM bandwidth_record
    WHERE time between '{}' AND '{}'
    AND GREATEST(r_strm, r_strm_filt) > 0
    GROUP BY node_id
    ORDER BY weight DESC, ratio
    LIMIT 30) as subquery
    """
    query = " intersect ".join([subquery] * 8).format(
        *list(sum(dt_intervals, ()))
    )
    logger.debug(f"{query}")
    cur.execute(query)
    rows = cur.fetchall()
    logger.info("Fetched %s relays.", cur.rowcount)
    relay_fps = list(map(lambda x: x[0], rows))
    return relay_fps


def fetch_greater_weight_stream(
    cur,
):
    query = """
    SELECT DISTINCT node_id
    FROM bandwidth_record
    WHERE time between '{}' AND '{}'
    GROUP BY node_id, bw, bw_mean
    HAVING max(bw_mean) * 200 < max(bw * 1000.0)
    AND bw_mean > -1;
    """.format(
        *config.date_interval
    )
    logger.debug(f"{query}")
    cur.execute(query)
    rows = cur.fetchall()
    logger.info("Fetched %s relays.", cur.rowcount)
    relay_fps = list(map(lambda x: x[0], rows))
    return relay_fps


# For tpo/network-health/sbws#40190:
# Low consensus weight and measurements for relays with high capacity
# -------------------------------------------------------------------
def fetch_similar_relays(
    cur,
    nick,
    autonomous_system,
):
    """Fetch similar relays by same ASN, exit and non exits."""
    similar_relays = {
        "top_exit_same_as": None,
        "top_exit_different_as": None,
        "top_non_exit_same_as": None,
        "top_non_exit_different_as": None,
    }

    time_filter = "AND published BETWEEN '{}' AND '{}'".format(
        *config.date_interval
    )
    order = """
    GROUP by node_id
    ORDER BY weight DESC
    LIMIT 1
    """
    SELECT = """
    SELECT
    DISTINCT node_id,
    avg(bw) / 1000 as weight
    FROM bandwidth_record
    INNER JOIN server_status ON server_status.fingerprint =
        substring(bandwidth_record.node_id,2)
    WHERE
    """
    query = f"""{SELECT}
    autonomous_system='{autonomous_system}'
    AND flags like '%Exit%'
    AND nick NOT like '%{nick}%'
    {time_filter} {order}
    """
    cur.execute(query)
    rows = cur.fetchall()
    logger.info("Fetched %s relays.", cur.rowcount)
    if rows:
        similar_relays["top_exit_same_as"] = rows[0][0]

    query = f"""{SELECT}
    NOT autonomous_system='{autonomous_system}'
    AND flags like '%Exit%'
    {time_filter} {order}
    """
    cur.execute(query)
    rows = cur.fetchall()
    logger.info("Fetched %s relays.", cur.rowcount)
    if rows:
        similar_relays["top_exit_different_as"] = rows[0][0]

    query = f"""{SELECT}
    autonomous_system='{autonomous_system}'
    AND flags NOT like '%Exit%'
    AND nick NOT like '%{nick}%'
    {time_filter} {order}
    """
    cur.execute(query)
    rows = cur.fetchall()
    logger.info("Fetched %s relays.", cur.rowcount)
    if rows:
        similar_relays["top_non_exit_same_as"] = rows[0][0]

    query = f"""{SELECT}
    NOT autonomous_system='{autonomous_system}'
    AND flags NOT like '%Exit%'
    {time_filter} {order}
    """
    cur.execute(query)
    rows = cur.fetchall()
    logger.info("Fetched %s relays.", cur.rowcount)
    if rows:
        similar_relays["top_non_exit_different_as"] = rows[0][0]
    logger.debug("top similar %s", similar_relays)
    return similar_relays
